import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AngularHelloWorld';
  message = {
    title: 'Message 1',
    text: 'This is message 1'
  };
}
